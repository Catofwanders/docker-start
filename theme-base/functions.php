<?php
/**
 * Theme Base functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @since theme-base
 * @package Base
 */

require get_template_directory() . '/lib/includes/theme.scripts-styles.php';
require get_template_directory() . '/lib/includes/theme.widgets.php';
if(!is_user_logged_in()){ require get_template_directory() . '/lib/includes/theme.clean-header.php'; }


function true_function(){
  echo 'work';
}
add_action('wp_ajax_myaction', 'true_function');
add_action('wp_ajax_nopriv_myaction', 'true_function'); 