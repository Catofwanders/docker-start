(function($) {
  $(document).ready(function() {
    // Global variables
    // var _window = $(window);
    // var _document = $(document);
    // READY - triggered when PJAX DONE
    function pageReady() {
      formSend();
    }
    // this is a master function which should have all functionality
    pageReady();
    // some plugins work best with onload triggers
    // _window.on('load', function() {
    // your functions
    // });
    // function formSend() {
    //   $.ajax({
    //     url: frontendajax.ajaxurl,
    //     type: 'POST',
    //     data: 'action=myaction&param1=2&param2=3', // можно также передать в виде объекта
    //     beforeSend: function(xhr) {

    //     },
    //     success: function(data) {
    //       console.log(data);
    //     }
    //   });
    // }
  });
})(jQuery);
